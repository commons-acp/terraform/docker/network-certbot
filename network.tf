resource "docker_network" "network_bridge" {
  count = var.network_name == "" ? 1 :0
  name = "tf-network"
  driver = "bridge"
}