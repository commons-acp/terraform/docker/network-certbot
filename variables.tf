variable "letsencrypt_docker_image" {
  default = "jrcs/letsencrypt-nginx-proxy-companion:latest"
}

variable "network_name" {
  default = ""
}

variable "proxy_docker_image" {
  default = "jwilder/nginx-proxy:latest"
}
variable "proxy_root_path" {
  default = "/home/docker-data/proxy"
}
