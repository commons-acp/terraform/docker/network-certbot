resource "docker_image" "letsencrypt" {
  name = var.letsencrypt_docker_image
  keep_locally = true
}

resource "docker_container" "letsencrypt" {
  depends_on = [docker_container.proxy]

  image = docker_image.letsencrypt.latest
  name = "letsencrypt"
  user = "root"
  must_run = true
  restart = "unless-stopped"

  env = [
  ]

  lifecycle {
    ignore_changes = [env, dns, dns_search, domainname, network_mode, working_dir, labels, cpu_shares, memory, memory_swap, tmpfs, user]
  }

  volumes {
    read_only = true
    container_path = "/var/run/docker.sock"
    host_path = "/var/run/docker.sock"
  }

  volumes {
    host_path = "${var.proxy_root_path}/certs/"
    container_path = "/etc/nginx/certs"
    read_only = false
  }

  volumes {
    from_container = "proxy"
  }


  networks_advanced {
    name = var.network_name == "" ?  docker_network.network_bridge[0].name : var.network_name
  }
}
