resource "docker_image" "proxy" {
  name = var.proxy_docker_image
  keep_locally = true
}

resource "docker_container" "proxy" {
  image = docker_image.proxy.latest
  name = "proxy"
  user = "root"
  must_run = true
  restart = "unless-stopped"

  env = [
  ]

  lifecycle {
    ignore_changes = [dns, dns_search, domainname, network_mode, working_dir, labels, cpu_shares, memory, memory_swap, tmpfs, user]
  }

  volumes {
    read_only = true
    container_path = "/tmp/docker.sock"
    host_path = "/var/run/docker.sock"
  }

  volumes {
    host_path = "${var.proxy_root_path}/conf.d/"
    container_path = "/etc/nginx/conf.d/"
    read_only = false
  }

  volumes {
    host_path = "${var.proxy_root_path}/vhost.d/"
    container_path = "/etc/nginx/vhost.d/"
    read_only = false
  }
  upload {
    source = "${path.module}/config/custom.conf"
    file = "/etc/nginx/conf.d/custom.conf"
  }
  volumes {
    host_path = "${var.proxy_root_path}/html/"
    container_path = "/usr/share/nginx/html"
    read_only = false
  }

  volumes {
    host_path = "${var.proxy_root_path}/certs/"
    container_path = "/etc/nginx/certs"
    read_only = false
  }

  ports {
    internal = 80
    external = 80
  }

  ports {
    internal = 443
    external = 443
  }

  networks_advanced {
    name = var.network_name == "" ?  docker_network.network_bridge[0].name : var.network_name
  }
}
