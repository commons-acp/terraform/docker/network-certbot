output "network_bridge_name" {
  value = docker_network.network_bridge[0].name
}
output "proxy_root_path" {
  value = var.proxy_root_path
}